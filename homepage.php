<?php
/**
* Template Name: Homepage
 */

get_header(); 

	// 0 Homepage
	if (is_front_page()):
	?>
		
		<!-- Mobile Home Hero Carousel -->
		<div id="mobileCarousel" class="carousel slide d-block d-md-none" data-ride="carousel" data-interval="6000">
		  <ol class="carousel-indicators">
		    <li data-target="#mobileCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#mobileCarousel" data-slide-to="1"></li>
		    <li data-target="#mobileCarousel" data-slide-to="2"></li>
		    <li data-target="#mobileCarousel" data-slide-to="3"></li>
		    <li data-target="#mobileCarousel" data-slide-to="4"></li>
		  </ol>
		  <div class="carousel-inner" role="listbox">
		    <div class="carousel-item active hero_slide1">
				<img class="d-block img-fluid" src="<?php the_field('10_hero_slide1_mobile') ?>" alt="First slide">
				<div class="hero_slide1_contents">
					<h4 class="carousel_subtitle"><?php the_field('10_hero_slide1_header_txt') ?></h4>
					<p class="carousel_txt"><?php the_field('10_hero_slide1_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide1_btn') ?></button>
				</div>
		    </div>
		    <div class="carousel-item hero_slide2">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide2_mobile') ?>" alt="Second slide">
		    	<div class="hero_slide2_contents">
			    	<h4 class="carousel_subtitle"><?php the_field('10_hero_slide2_header_txt') ?></h4>
					<p class="carousel_txt"><?php the_field('10_hero_slide2_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide2_btn') ?></button>
				</div>
		    </div>
		    <div class="carousel-item hero_slide3">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide3_mobile') ?>" alt="Third slide">
		    	<h4 class="slide3_subtitle"><?php the_field('10_hero_slide3_header1') ?></h4>
				<h1 class="slide3_header"><?php the_field('10_hero_slide3_header2') ?></h1>
		    	<div class="hero_slide3_contents">
					<p class="carousel_txt"><?php the_field('10_hero_slide3_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide3_btn') ?></button>
		    	</div>
		    </div>
		    <div class="carousel-item hero_slide4">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide4_mobile') ?>" alt="Fourth slide">
		    	<h1 class="slide4_header"><?php the_field('10_hero_slide4_header') ?></h1>
		    	<div class="hero_slide4_contents">
			    	<h4 class="carousel_subtitle"><?php the_field('10_hero_slide4_header_txt') ?></h4>
			    	<p class="carousel_txt"><?php the_field('10_hero_slide4_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide4_btn') ?></button>
		    	</div>
		    </div>
		    <div class="carousel-item hero_slide5">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide5_mobile') ?>" alt="Fifth slide">
		    	<h1 class="slide5_header"><?php the_field('10_hero_slide5_header') ?></h1>
		    	<div class="hero_slide5_contents">
			    	<h4 class="carousel_subtitle"><?php the_field('10_hero_slide5_header_txt') ?></h4>
					<p class="carousel_txt"><?php the_field('10_hero_slide5_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide5_btn') ?></button>
		    	</div>
		    </div>
		  </div>
		</div>
		
		<!-- Tablet-Laptop-Desktop Home Hero Carousel -->
		<div id="homeCarousel" class="carousel slide d-none d-md-block" data-ride="carousel" data-interval="6000">
		  <ol class="carousel-indicators">
		    <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#homeCarousel" data-slide-to="1"></li>
		    <li data-target="#homeCarousel" data-slide-to="2"></li>
		    <li data-target="#homeCarousel" data-slide-to="3"></li>
		    <li data-target="#homeCarousel" data-slide-to="4"></li>
		  </ol>
		  <div class="carousel-inner" role="listbox">
		    <div class="carousel-item active hero_slide1">
				<img class="d-block img-fluid" src="<?php the_field('10_hero_slide1') ?>" alt="First slide">
				<div class="hero_slide1_contents">
					<h4 class="carousel_subtitle"><?php the_field('10_hero_slide1_header_txt') ?></h4>
					<p class="carousel_txt"><?php the_field('10_hero_slide1_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide1_btn') ?></button>
				</div>
		    </div>
		    <div class="carousel-item hero_slide2">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide2') ?>" alt="Second slide">
		    	<div class="hero_slide2_contents">
			    	<h4 class="carousel_subtitle"><?php the_field('10_hero_slide2_header_txt') ?></h4>
					<p class="carousel_txt"><?php the_field('10_hero_slide2_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide2_btn') ?></button>
				</div>
		    </div>
		    <div class="carousel-item hero_slide3">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide3') ?>" alt="Third slide">
		    	<h4 class="slide3_subtitle"><?php the_field('10_hero_slide3_header1') ?></h4>
		    	<div class="hero_slide3_contents">
					<p class="carousel_txt"><?php the_field('10_hero_slide3_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide3_btn') ?></button>
		    	</div>
		    </div>
		    <div class="carousel-item hero_slide4">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide4') ?>" alt="Fourth slide">
		    	<h1 class="slide4_header"><?php the_field('10_hero_slide4_header') ?></h1>
		    	<div class="hero_slide4_contents">
			    	<h4 class="carousel_subtitle"><?php the_field('10_hero_slide4_header_txt') ?></h4>
			    	<p class="carousel_txt"><?php the_field('10_hero_slide4_txt') ?></p>
		    	</div>
		    	<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide4_btn') ?></button>
		    </div>
		    <div class="carousel-item hero_slide5">
		    	<img class="d-block img-fluid" src="<?php the_field('10_hero_slide5') ?>" alt="Fifth slide">
		    	<h1 class="slide5_header"><?php the_field('10_hero_slide5_header') ?></h1>
		    	<div class="hero_slide5_contents">
			    	<h4 class="carousel_subtitle"><?php the_field('10_hero_slide5_header_txt') ?></h4>
					<p class="carousel_txt"><?php the_field('10_hero_slide5_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_hero_slide5_btn') ?></button>
		    	</div>
		    </div>
		  </div>
		</div>
		
		<script>		
			jQuery(document).ready(function() {
				
				// Enable swiping...
				jQuery(".carousel-inner").swipe( {
					// Generic swipe handler for all directions
					swipeLeft:function(event, direction, distance, duration, fingerCount) {
						jQuery(this).parent().carousel('next'); 
					},
					swipeRight: function() {
						jQuery(this).parent().carousel('prev'); 
					},
					// Default is 75px, set to 0 for demo so any distance triggers swipe
					threshold:0
				});
					
			});
		</script>
		
		<!-- Home Section1 -->
		<div class="home_sec1_bg d-block d-md-none">
			<div class="home_sec1_contents">
				<h1 class="sec1_header"><?php the_field('10_sec1_header_txt') ?></h1>
				<form action="#" method="POST">
					<label class="sec_label" for="">Join Our Mailing List</label><br />
					<input class="sec_input" type="text" name="email" placeholder="Enter email address" /><input class="sec_submit" type="submit" name="mailing_list" value="Submit" />
				</form>
			</div>
		</div>
		
		<div class="home_sec1_bg d-none d-md-block">
			<img src="http://64.207.185.148/wp-content/uploads/2018/06/home_sec1_bg.png" alt="Home Section1 BG" />
			<div class="home_sec1_contents">
				<h1 class="sec1_header"><?php the_field('10_sec1_header_txt') ?></h1>
				<form action="#" method="POST">
					<label class="sec_label" for="">Join Our Mailing List</label>
					<input class="sec_input" type="text" name="email" placeholder="Enter email address" /><input class="sec_submit" type="submit" name="mailing_list" value="Submit" />
				</form>
			</div>
		</div>
		
		<!-- Home Section2 -->
		<div class="home_sec2_bg">
			<img class="d-none d-md-block" src="http://64.207.185.148/wp-content/uploads/2018/06/home_sec2_bg.png" alt="Home Section2 BG" />
			<div class="row home_sec2-1_contents">
				<div class="col-12 col-md-6">
					<img src="<?php the_field('10_sec2-1_img') ?>" alt="Home Section2 BG" />
				</div>
				<div class="col-12 col-md-6 sec_contents">
					<h1 class="sec_header"><?php the_field('10_sec2-1_header') ?></h1>
			    	<h4 class="sec_subtitle"><?php the_field('10_sec2-1_header_txt') ?></h4>
					<p class="sec_txt"><?php the_field('10_sec2-1_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_sec2-1_btn') ?></button>
				</div>
			</div>
			<div class="row home_sec2-2_contents">
				<div class="col-12 sec_contents">
					<h1 class="sec_header"><?php the_field('10_sec2-2_header') ?></h1>
			    	<h4 class="sec_subtitle"><?php the_field('10_sec2-2_header_txt') ?></h4>
					<p class="sec_txt"><?php the_field('10_sec2-2_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_sec2-2_btn') ?></button>
				</div>
			</div>
			<div class="row home_sec2-3_contents">
				<div class="col-12 col-md-6">
					<img src="<?php the_field('10_sec2-3_img') ?>" alt="Home Section2 BG" />					
				</div>
				<div class="col-12 col-md-6 sec_contents">
					<h1 class="sec_header"><?php the_field('10_sec2-3_header') ?></h1>
			    	<h4 class="sec_subtitle"><?php the_field('10_sec2-3_header_txt') ?></h4>
					<p class="sec_txt"><?php the_field('10_sec2-3_txt') ?></p>
					<button onclick="window.location.href = '#';"><?php the_field('10_sec2-3_btn') ?></button>
				</div>
			</div>
		</div>
		
		<!-- Home Section3 -->
		<div class="home_sec3_bg d-block d-md-none">
			<div class="home_sec3_contents">
				<h1 class="sec_header"><?php the_field('10_sec3_header_txt') ?></h1>
				<p class="sec_txt"><?php the_field('10_sec3_txt') ?></p>
				<button onclick="window.location.href = '#';"><?php the_field('10_sec3_btn') ?></button>
			</div>
		</div>
		<div class="home_sec3_bg d-none d-md-block">
			<img src="<?php the_field('10_sec3_bg') ?>" alt="Home Section3 BG" />
			<div class="home_sec3_contents">
				<h1 class="sec_header"><?php the_field('10_sec3_header_txt') ?></h1>
				<p class="sec_txt"><?php the_field('10_sec3_txt') ?></p>
				<button onclick="window.location.href = '#';"><?php the_field('10_sec3_btn') ?></button>
			</div>
		</div>
		
		<!-- Home Section4 -->
		<div class="home_sec4_wrapper">
			<div class="row home_sec4">
				<div class="col-12">
					<h1 class="sec_header"><?php the_field('10_sec4_header_txt') ?></h1>
					<p class="sec_txt"><?php the_field('10_sec4_txt') ?></p>
				</div>
			</div>
			
			<div class="row home_sec4_contents">
				<div class="col-12 col-md-4 home_sec4_col">
					<img src="<?php the_field('10_sec4-1_img') ?>" alt="Home Section4-1 Image" />
					<h4 class="sec4_subtitle"><a class="sec_link" href="#"><?php the_field('10_sec4-1_header_txt') ?></a></h4>
					<p class="sec_txt"><?php the_field('10_sec4-1_txt') ?></p>
				</div>
				<div class="col-12 col-md-4 home_sec4_col">
					<img src="<?php the_field('10_sec4-2_img') ?>" alt="Home Section4-2 Image" />
					<h4 class="sec4_subtitle"><a class="sec_link" href="#"><?php the_field('10_sec4-2_header_txt') ?></a></h4>
					<p class="sec_txt"><?php the_field('10_sec4-2_txt') ?></p>
				</div>
				<div class="col-12 col-md-4 home_sec4_col">
					<img src="<?php the_field('10_sec4-3_img') ?>" alt="Home Section4-3 Image" />
					<h4 class="sec4_subtitle"><a class="sec_link" href="#"><?php the_field('10_sec4-3_header_txt') ?></a></h4>
					<p class="sec_txt"><?php the_field('10_sec4-3_txt') ?></p>
				</div>
			</div>
		</div>
		
		<!-- Home Section5 -->
		<div class="home_sec5_bg d-block d-md-none">
			<div class="home_sec5_contents">
				<h1 class="sec_header"><?php the_field('10_sec5_header_txt') ?></h1>
				<p class="sec_txt"><?php the_field('10_sec5_txt') ?></p>
				<button onclick="window.location.href = '#';"><?php the_field('10_sec5_btn') ?></button>
			</div>
		</div>
		
		<div class="home_sec5_bg d-none d-md-block">
			<img src="http://64.207.185.148/wp-content/uploads/2018/06/home_sec5_bg.png" alt="Home Section5 BG" />
			<div class="home_sec5_contents">
				<h1 class="sec_header"><?php the_field('10_sec5_header_txt') ?></h1>
				<p class="sec_txt"><?php the_field('10_sec5_txt') ?></p>
				<button onclick="window.location.href = '#';"><?php the_field('10_sec5_btn') ?></button>
			</div>
		</div>
			
	<?php
	endif;

get_footer();
?>