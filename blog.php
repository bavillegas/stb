<?php
/**
* Template Name: Blog
 */

get_header(); 

    // 6 Bay Stories
	if (is_page('326')):
		?>
		
		<div class="bst_container">
			<div class="col-12 bst_hero_wrapper">
				<img src="<?php the_field('326_hero_img'); ?>" alt="Bay Stories Hero Image" />
				<div class="bst_hero_header">
					<h1 class="hero_h1"><?php the_field('326_hero_header_txt'); ?></h1>
				</div>
				<div class="bst_hero_txt">
					<p class="freight-tp hero_p"><?php the_field('326_hero_txt'); ?></p>
				</div>
			</div>
		</div>
	
		<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-8">
			<main id="main" class="site-main" role="main">
	
				<?php
				$myposts = get_posts('');
				foreach($myposts as $post):
					setup_postdata($post);
				?>
					<div class="post-item">
						<div class="post-info">
							<h2 class="freight-sp post-title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_title(); ?>
								</a>
							</h2>
							<ul class="freight-sp blog-desc">
								<li><span class="dot"></span><?php the_author(); ?></li>
								<li><span class="dot"></span><?php the_category(); ?></li>
								<li><span class="dot"></span><?php echo get_the_date(); ?></li>
							</ul>
						</div>
						<div class="row post-bbox">
							<div class="col-sm-12 col-md-6 featured-img">
								<img src="<?php the_post_thumbnail_url(); ?>" alt="Featured Image" width="290px" />
							</div>
							<div class="col-sm-12 col-md-6 freight-tp post-content">
								<?php the_content('<button class="freight-sp blog_btn">Read More</button>'); ?>
							</div>
						</div>
						<hr class="post-hr" />
					</div>
				<?php 
				endforeach; 
				wp_reset_postdata(); 
				?>
	
			</main><!-- #main -->
		</section><!-- #primary -->
	
		<?php	
	endif;

get_sidebar();
get_footer();
?>