<?php
/**
* Template Name: Landing Page
 */

get_header();
		
		// 1 Who We Are
		if (is_page('191')):
		?>
			<div class="wwa_container">
				<div class="col-12 wwa_hero_wrapper">
					<img src="<?php the_field('191_hero_img'); ?>" alt="Who We Are Hero Image" />
					<div class="wwa_hero_header">
						<h1 class="hero_header"><?php the_field('191_hero_header_txt'); ?></h1>
					</div>
					<div class="wwa_hero_txt">
						<p class="hero_txt"><?php the_field('191_hero_txt'); ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 wwa_sec1_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/06/191_sec1_bg.png" alt="Who We Are Section1 BG" />
						<div class="wwa_sec1_contents">
							<h1 class="sec_header"><?php the_field('191_sec1_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('191_sec1_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('191_sec1_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 wwa_sec2_bg">
						<img class="wwa_sec2_photo" src="<?php the_field('191_sec2_img'); ?>" alt="Who We Are Section2 Image" />
						<div class="wwa_sec2_contents">
							<h1 class="sec_header"><?php the_field('191_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('191_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('191_sec2_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 wwa_sec2_bg">
						<img class="wwa_sec2_shape" src="https://dev.savesfbay.org/wp-content/uploads/2018/06/191_sec2_shape.png" alt="Who We Are Section2 Shape" />
						<div class="wwa_sec2_contents">
							<h1 class="sec_header"><?php the_field('191_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('191_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('191_sec2_btn') ?></button>
						</div>
						<img class="wwa_sec2_photo" src="<?php the_field('191_sec2_img'); ?>" alt="Who We Are Section2 Image" />
					</div>
				</div>
			
				<div class="row wwa_sec3">
					<div class="col-12 col-md-6 wwa_sec3_bg">
						<img src="<?php the_field('191_sec3_img'); ?>" alt="Who We Are Section3 Image" />
					</div>
					<div class="col-12 col-md-6 wwa_sec3_contents">
						<h1 class="sec_header"><?php the_field('191_sec3_header_txt'); ?></h1>
						<p class="sec_txt"><?php the_field('191_sec3_txt'); ?></p>
						<button onclick="window.location.href = '#';"><?php the_field('191_sec3_btn') ?></button>
					</div>
				</div>
			</div>
		
		<?php
		// 2 What We Do
		elseif (is_page('199')):
		?>
		
			<div class="wwd_container">
				<div class="col-12 wwd_hero_wrapper">
					<img src="<?php the_field('199_hero_img'); ?>" alt="What We Do Hero Image" />
					<div class="wwd_hero_header">
						<h1 class="hero_header"><?php the_field('199_hero_header_txt'); ?></h1>
					</div>
					<div class="wwd_hero_txt">
						<p class="hero_txt"><?php the_field('199_hero_txt'); ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 wwd_sec1_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/06/199_sec1_bg.png" alt="Who We Do Section1 BG" />
						<div class="wwd_sec1_contents">
							<h1 class="sec_header"><?php the_field('199_sec1_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec1_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec1_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 wwd_sec2_bg">
						<img class="wwd_sec2_photo" src="<?php the_field('199_sec2_img'); ?>" alt="What We Do Section2 Image" />
						<div class="wwd_sec2_contents">
							<h1 class="sec_header"><?php the_field('199_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec2_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 wwd_sec2_bg">
						<img class="wwd_sec2_shape" src="https://dev.savesfbay.org/wp-content/uploads/2018/06/199_sec2_shape.png" alt="What We Do Section2 Shape" />
						<div class="wwd_sec2_contents">
							<h1 class="sec_header"><?php the_field('199_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec2_btn') ?></button>
						</div>
						<img class="wwd_sec2_photo" src="<?php the_field('199_sec2_img'); ?>" alt="What We Do Section2 Image" />
					</div>
				</div>
			
				<div class="row d-block d-md-none">
					<div class="col-12 wwd_sec3_bg">
						<div class="wwd_sec3_contents">
							<h1 class="sec_header"><?php the_field('199_sec3_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec3_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec3_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 wwd_sec3_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/199_sec3_bg.png" alt="What We Do Section3 BG" />
						<div class="wwd_sec3_contents">
							<h1 class="sec_header"><?php the_field('199_sec3_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec3_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec3_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 wwd_sec4_bg">
						<div class="wwd_sec4_contents">
							<h1 class="sec_header"><?php the_field('199_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec4_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec4_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 wwd_sec4_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/199_sec4_bg.png" alt="What We Do Section4 BG" />
						<div class="wwd_sec4_contents">
							<h1 class="sec_header"><?php the_field('199_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec4_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec4_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 wwd_sec5_bg">
						<div class="wwd_sec5_contents">
							<h1 class="sec_header"><?php the_field('199_sec5_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec5_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec5_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 wwd_sec5_bg">
						<img src="<?php the_field('199_sec5_img'); ?>" alt="What We Do Section5 Image" />
						<div class="wwd_sec5_contents">
							<h1 class="sec_header"><?php the_field('199_sec5_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('199_sec5_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('199_sec5_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row wwd_sec6">
					<div class="col-12 col-md-6 wwd_sec6_contents">
						<h1 class="sec_header"><?php the_field('199_sec6_header_txt'); ?></h1>
						<p class="sec_txt"><?php the_field('199_sec6_txt'); ?></p>
						<button onclick="window.location.href = '#';"><?php the_field('199_sec6_btn') ?></button>
					</div>
					<div class="col-12 col-md-6 wwd_sec6_bg">
						<img src="<?php the_field('199_sec6_img'); ?>" alt="What We Do Section6 Image" />
					</div>
				</div>
			</div>
			
		<?php
		// 3 Impact
		elseif (is_page('203')):
				?>
		
			<div class="imp_container">
				<div class="col-12 imp_hero_wrapper">
					<img src="<?php the_field('203_hero_img'); ?>" alt="Impact Hero Image" />
					<div class="imp_hero_header">
						<h1 class="hero_header"><?php the_field('203_hero_header_txt'); ?></h1>
					</div>
					<div class="imp_hero_txt">
						<p class="hero_txt"><?php the_field('203_hero_txt'); ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 imp_sec1_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/203_sec1_bg.png" alt="Impact Section1 BG" />
						<div class="imp_sec1_contents">
							<h1 class="sec_header"><?php the_field('203_sec1_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec1_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec1_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 imp_sec2_bg">
						<div class="imp_sec2_contents">
							<h1 class="sec_header"><?php the_field('203_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec2_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block imp_sec2">
					<div class="col-12 imp_sec2_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/203_sec2_bg.png" alt="Impact Section2 BG" />
						<div class="imp_sec2_contents">
							<h1 class="sec_header"><?php the_field('203_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec2_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 imp_sec3_bg">
						<div class="imp_sec3_contents">
							<h1 class="sec_header"><?php the_field('203_sec3_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec3_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec3_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 imp_sec3_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/203_sec3_bg.png" alt="Impact Section3 BG" />
						<div class="imp_sec3_contents">
							<h1 class="sec_header"><?php the_field('203_sec3_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec3_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec3_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 imp_sec4_bg">
						<img class="imp_sec4_photo" src="<?php the_field('203_sec4_img'); ?>" alt="Impact Section4 Image" />
						<div class="imp_sec4_contents">
							<h1 class="sec_header"><?php the_field('203_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec2_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 imp_sec4_bg">
						<img class="imp_sec4_shape" src="https://dev.savesfbay.org/wp-content/uploads/2018/07/203_sec4_shape.png" alt="Impact Section4 Shape" />
						<div class="imp_sec4_contents">
							<h1 class="sec_header"><?php the_field('203_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('203_sec4_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('203_sec4_btn') ?></button>
						</div>
						<img class="imp_sec4_photo" src="<?php the_field('203_sec4_img'); ?>" alt="Impact Section4 Image" />
					</div>
				</div>
				
				<div class="row imp_sec5">
					<div class="col-12 col-md-6 imp_sec5_bg">
						<img src="<?php the_field('203_sec5_img'); ?>" alt="Get Involved Section5 Image" />
					</div>
					<div class="col-12 col-md-6 imp_sec5_contents">
						<h1 class="sec_header"><?php the_field('203_sec5_header_txt'); ?></h1>
						<p class="sec_txt"><?php the_field('203_sec5_txt'); ?></p>
						<button onclick="window.location.href = '#';"><?php the_field('203_sec5_btn') ?></button>
					</div>
				</div>
			</div>
			
		<?php
		// 4 Experience Your Bay
		elseif (is_page('246')):
			?>
		
			<div class="eyb_container">
				<div class="col-12 eyb_hero_wrapper">
					<img src="<?php the_field('246_hero_img'); ?>" alt="Impact Hero Image" />
					<div class="eyb_hero_header">
						<h1 class="hero_header"><?php the_field('246_hero_header_txt'); ?></h1>
					</div>
					<div class="eyb_hero_txt">
						<p class="hero_txt"><?php the_field('246_hero_txt'); ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 eyb_sec1_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/246_sec1_bg.png" alt="Experience Your Bay Section1 BG" />
						<div class="eyb_sec1_contents">
							<h1 class="sec_header"><?php the_field('246_sec1_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('246_sec1_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('246_sec1_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 eyb_sec2_bg">
						<img class="eyb_sec2_photo" src="<?php the_field('246_sec2_img'); ?>" alt="Experience Your Bay Section2 Image" />
						<div class="eyb_sec2_contents">
							<h1 class="sec_header"><?php the_field('246_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('246_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('246_sec2_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block eyb_sec2">
					<div class="col-12 eyb_sec2_bg">
						<img class="eyb_sec2_shape" src="https://dev.savesfbay.org/wp-content/uploads/2018/07/246_sec2_shape.png" alt="Experience Your Bay Section2 Shape" />
						<div class="eyb_sec2_contents">
							<h1 class="sec_header"><?php the_field('246_sec2_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('246_sec2_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('246_sec2_btn') ?></button>
						</div>
						<img class="eyb_sec2_photo" src="<?php the_field('246_sec2_img'); ?>" alt="Experience Your Bay2 Image" />
					</div>
				</div>
				
				<div class="row eyb_sec3">
					<div class="col-12 col-md-6 eyb_sec3_bg">
						<img class="eyb_sec2_shape" src="https://dev.savesfbay.org/wp-content/uploads/2018/07/246_sec3_img.png" alt="Experience Your Bay Section3 Image" />
					</div>
					<div class="col-12 col-md-6 eyb_sec3_contents">
						<h1 class="sec_header"><?php the_field('246_sec3_header_txt'); ?></h1>
						<p class="sec_txt"><?php the_field('246_sec3_txt'); ?></p>
						<button onclick="window.location.href = '#';"><?php the_field('246_sec3_btn') ?></button>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 eyb_sec4_bg">
						<div class="eyb_sec4_contents">
							<h1 class="sec_header"><?php the_field('246_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('246_sec4_txt'); ?></p>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 eyb_sec4_bg">
						<img class="eyb_sec2_shape" src="https://dev.savesfbay.org/wp-content/uploads/2018/07/246_sec4_bg.png" alt="Experience Your Bay Section4 Image" />
						<div class="eyb_sec4_contents">
							<h1 class="sec_header"><?php the_field('246_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('246_sec4_txt'); ?></p>
						</div>
					</div>
				</div>
			</div>
			
		<?php		
		// 5 Get Involved
		elseif (is_page('209')):
		?>
		
			<div class="gin_container">
				<div class="col-12 gin_hero_wrapper">
					<img src="<?php the_field('209_hero_img'); ?>" alt="Get Involved Hero Image" />
					<div class="gin_hero_header">
						<h1 class="hero_header"><?php the_field('209_hero_header_txt'); ?></h1>
					</div>
					<div class="gin_hero_txt">
						<p class="hero_txt"><?php the_field('209_hero_txt'); ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 gin_sec1_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/209_sec1_bg.png" alt="Get Involved Section1 BG" />
						<div class="gin_sec1_contents">
							<h1 class="sec_header"><?php the_field('209_sec1_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('209_sec1_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('209_sec1_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row gin_sec2">
					<div class="col-12 col-md-6 gin_sec2_contents">
						<h1 class="sec_header"><?php the_field('209_sec2_header_txt'); ?></h1>
						<p class="sec_txt"><?php the_field('209_sec2_txt'); ?></p>
						<button onclick="window.location.href = '#';"><?php the_field('209_sec2_btn') ?></button>
					</div>
					<div class="col-12 col-md-6 gin_sec2_bg">
						<img src="<?php the_field('209_sec2_img'); ?>" alt="Who We Are Section3 Image" />
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 gin_sec3_bg">
						<div class="gin_sec3_contents">
							<h1 class="sec_header"><?php the_field('209_sec3_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('209_sec3_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('209_sec3_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block gin_sec3">
					<div class="col-12 gin_sec3_bg">
						<img src="https://dev.savesfbay.org/wp-content/uploads/2018/07/209_sec3_bg.png" alt="Get Involved Section3 BG" />
						<div class="gin_sec3_contents">
							<h1 class="sec_header"><?php the_field('209_sec3_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('209_sec3_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('209_sec3_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-block d-md-none">
					<div class="col-12 gin_sec4_bg">
						<div class="gin_sec4_contents">
							<h1 class="sec_header"><?php the_field('209_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('209_sec4_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('209_sec4_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row d-none d-md-block">
					<div class="col-12 gin_sec4_bg">
						<img src="<?php the_field('209_sec4_img'); ?>" alt="Get Involved Section4 BG" />
						<div class="gin_sec4_contents">
							<h1 class="sec_header"><?php the_field('209_sec4_header_txt'); ?></h1>
							<p class="sec_txt"><?php the_field('209_sec4_txt'); ?></p>
							<button onclick="window.location.href = '#';"><?php the_field('209_sec4_btn') ?></button>
						</div>
					</div>
				</div>
				
				<div class="row gin_sec5">
					<div class="col-12 col-md-6 gin_sec5_bg">
						<img src="<?php the_field('209_sec5_img'); ?>" alt="Get Involved Section5 Image" />
					</div>
					<div class="col-12 col-md-6 gin_sec5_contents">
						<h1 class="sec_header"><?php the_field('209_sec5_header_txt'); ?></h1>
						<p class="sec_txt"><?php the_field('209_sec5_txt'); ?></p>
						<button onclick="window.location.href = '#';"><?php the_field('209_sec5_btn') ?></button>
					</div>
				</div>
			</div>
			
			<?php
		endif;	

get_footer();
?>